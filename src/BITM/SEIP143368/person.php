<?php
namespace app;
class Person
{
    public $name="Rahad";
    public $gender="male";
    public $blood_group="A+";

    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}